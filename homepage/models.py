from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    # Fields
    status_field = models.TextField(max_length=300)
    date_field = models.DateTimeField(auto_now=True)